<?php

return[
    'wage' => [
        'amount' => (int) env('DEFAULT_TRANSFER_COMMISSION', 0),
    ],
    'sms_provider' => [
        'ghasedak' => [
            'api_key' => env('GASEDAK_API_KEY'),
            'lineNumber' => env('GASEDAK_LINENUMBER'),
        ],
        'kavenegar' => [
            'api_key' => env('KAVENEGAR_API_KEY'),
            'lineNumber' => env('KAVENEGAR_LINENUMBER'),
        ]
    ]
];
