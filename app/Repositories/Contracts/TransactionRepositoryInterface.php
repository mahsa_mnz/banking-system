<?php

namespace App\Repositories\Contracts;

use App\Enum\TransactionRequestType;
use App\Models\Transaction;
use Illuminate\Database\Eloquent\Builder;

interface TransactionRepositoryInterface
{
    public function createWithdrawTransaction(
        int $cardId,
        string $cardNumber,
        int $amount,
        int $preBalance,
        int $afterBalance,
        int $commission = 0,
        $request_type = TransactionRequestType::WITHDRAW
    ): Transaction;

    public function createDepositTransaction(
        int $cardId,
        string $cardNumber,
        int $amount,
        int $preBalance,
        int $afterBalance,
        int $referenceId = null
    ): Transaction;

    public function getLatestTransactionsQuery(int $lastMinutes): Builder;
}
