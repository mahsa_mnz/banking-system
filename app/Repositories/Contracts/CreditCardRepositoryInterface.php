<?php

namespace App\Repositories\Contracts;

use App\Models\CreditCard;

interface CreditCardRepositoryInterface
{
    public function findByNumber(string $cardNumber, bool $fail = false, bool $eagerLoad = false): CreditCard|null;
}
