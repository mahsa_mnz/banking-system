<?php

namespace App\Repositories\Contracts;

use App\Models\MessageTemplate;

interface MessageTemplateRepositoryInterface
{
    public function findByName($templateName): MessageTemplate;
}
