<?php

namespace App\Repositories\Contracts;

use App\Models\Account;
use Illuminate\Database\Eloquent\Collection;

interface AccountRepositoryInterface
{
    public function getAccountWithId(int $id, bool $lock = false): Account;

    public function getAccountsWithId(array $ids, bool $lock = false): Collection|array;

    public function updateBalance(int $id, int $newBalance): int;
}
