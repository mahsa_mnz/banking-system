<?php

namespace App\Repositories\Contracts;

use Illuminate\Database\Eloquent\Collection;

interface UserRepositoryInterface
{
    public function findUserByMobile(string $mobile, bool $fail = false);

    public function getTopUserIdsWithMostLatestTransactions(int $usersCount, int $lastMinutes): array;

    public function getTopUsersWithLatestTransactions(
        int $usersCount,
        $transactionsCount,
        int $lastMinutes
    ): Collection|array;
}
