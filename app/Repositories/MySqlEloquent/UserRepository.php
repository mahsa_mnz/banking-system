<?php

namespace App\Repositories\MySqlEloquent;

use App\Models\User;
use App\Repositories\Contracts\TransactionRepositoryInterface;
use App\Repositories\Contracts\UserRepositoryInterface;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Query\JoinClause;
use Illuminate\Support\Facades\DB;

class UserRepository implements UserRepositoryInterface
{

    public function __construct(private User $model)
    {
    }

    public function findUserByMobile(string $mobile, bool $fail = false): User|null
    {
        /** @var User $user */
        $query = $this->model::query()->where('mobile', $mobile);
        if ($fail) {
            $user = $query->firstOrFail();
        } else {
            $user = $query->first();
        }
        return $user;
    }

    public function getTopUserIdsWithMostLatestTransactions(int $usersCount, int $lastMinutes): array
    {
        $latestTransactions = app(TransactionRepositoryInterface::class)
            ->getLatestTransactionsQuery($lastMinutes);

        return $this->model::query()
            ->select('user_id', DB::raw('Count(*) as transactions_count'))
            ->join('accounts', 'users.id', '=', 'accounts.user_id')
            ->join('credit_cards', 'accounts.id', '=', 'credit_cards.account_id')
            ->joinSub($latestTransactions, 'latestTransactions', function (JoinClause $join) {
                $join->on('credit_cards.id', '=', 'latestTransactions.credit_card_id');
            })
            ->groupBy('user_id')
            ->orderByDesc('transactions_count')
            ->limit($usersCount)
            ->pluck('transactions_count', 'user_id')
            ->toArray();
    }

    public function getTopUsersWithLatestTransactions(
        int $usersCount,
        $transactionsCount,
        int $lastMinutes
    ): Collection|array {
        $users_transactions = $this->getTopUserIdsWithMostLatestTransactions($usersCount, $lastMinutes);

        $result = $this->model::query()
            ->with(['transactions'])
            ->whereIn('id', array_keys($users_transactions))
            ->get();

        return $result->map(function ($user) use ($transactionsCount) {
            $user->setRelation('transactions', $user->transactions->take($transactionsCount));
            return $user;
        });
    }
}
