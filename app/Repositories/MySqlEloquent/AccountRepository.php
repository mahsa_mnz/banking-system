<?php

namespace App\Repositories\MySqlEloquent;

use App\Models\Account;
use App\Repositories\Contracts\AccountRepositoryInterface;
use Illuminate\Database\Eloquent\Collection;

class AccountRepository implements AccountRepositoryInterface
{

    public function __construct(private Account $model)
    {
    }

    public function getAccountWithId(int $id, bool $lock = false): Account
    {
        $query = $this->model::query()->where('id', $id);

        if ($lock) {
            $query = $query->lock();
        }

        return $query->firstOrFail();
    }

    public function getAccountsWithId(array $ids, bool $lock = false): Collection|array
    {
        $query = $this->model::query()->whereIn('id', $ids);

        if ($lock) {
            $query = $query->lock();
        }

        return $query->get();
    }

    public function updateBalance(int $id, int $newBalance): int
    {
        return $this->model::query()->where('id', $id)
            ->update([
                'balance' => $newBalance
            ]);
    }
}
