<?php

namespace App\Repositories\MySqlEloquent;

use App\Models\CreditCard;
use App\Repositories\Contracts\CreditCardRepositoryInterface;

class CreditCardRepository implements CreditCardRepositoryInterface
{

    public function __construct(private CreditCard $model)
    {
    }

    public function findByNumber(string $cardNumber, bool $fail = false, bool $eagerLoad = false): CreditCard|null
    {
        /** @var CreditCard $creditCard */
        $query = $this->model::query()->where('number', $cardNumber);

        if ($eagerLoad) {
            $query = $query->with(['account', 'account.owner']);
        }

        if ($fail) {
            $creditCard = $query->firstOrFail();
        } else {
            $creditCard = $query->first();
        }

        return $creditCard;
    }
}
