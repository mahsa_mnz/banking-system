<?php

namespace App\Repositories\MySqlEloquent;

use App\Models\MessageTemplate;
use App\Repositories\Contracts\MessageTemplateRepositoryInterface;

class MessageTemplateRepository implements MessageTemplateRepositoryInterface
{
    public function __construct(private MessageTemplate $model)
    {
    }

    public function findByName($templateName): MessageTemplate
    {
        return $this->model::query()->where('template_name', $templateName)->firstOrFail();
    }
}
