<?php

namespace App\Repositories\MySqlEloquent;

use App\Enum\TransactionRequestType;
use App\Enum\TransactionType;
use App\Models\Transaction;
use App\Repositories\Contracts\TransactionRepositoryInterface;
use Illuminate\Database\Eloquent\Builder;

class TransactionRepository implements TransactionRepositoryInterface
{
    public function __construct(private Transaction $model)
    {
    }

    public function createWithdrawTransaction(
        int $cardId,
        string $cardNumber,
        int $amount,
        int $preBalance,
        int $afterBalance,
        int $commission = 0,
        $request_type = TransactionRequestType::WITHDRAW
    ): Transaction {
        /** @var Transaction $transaction */
        $transaction = $this->model::query()->create([
            'transaction_type' => TransactionType::DECREASE,
            'request_type' => $request_type,
            'credit_card_id' => $cardId,
            'credit_card_number' => $cardNumber,
            'transaction_amount' => $amount,
            'balance_before_tr' => $preBalance,
            'balance_after_tr' => $afterBalance,
        ]);

        if ($commission > 0) {
            $transaction->commission()->create(['amount' => $commission]);
        }

        return $transaction;
    }

    public function createDepositTransaction(
        int $cardId,
        string $cardNumber,
        int $amount,
        int $preBalance,
        int $afterBalance,
        int $referenceId = null
    ): Transaction {
        /** @var Transaction $transaction */
        $transaction =  $this->model::query()->create([
            'transaction_type' => TransactionType::INCREASE,
            'request_type' => $referenceId != null ? TransactionRequestType::TRANSFER : TransactionRequestType::DEPOSIT,
            'reference_transaction_id' => $referenceId,
            'credit_card_id' => $cardId,
            'credit_card_number' => $cardNumber,
            'transaction_amount' => $amount,
            'balance_before_tr' => $preBalance,
            'balance_after_tr' => $afterBalance,
        ]);

        return $transaction;
    }

    public function getLatestTransactionsQuery(int $lastMinutes): Builder
    {
        return $this->model::query()
            ->where('created_at', '>=', now()->subMinutes($lastMinutes)->format('Y-m-d H:i:s'));
    }
}
