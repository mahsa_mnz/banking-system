<?php

namespace App\Models;

use App\Enum\TransactionRequestType;
use App\Enum\TransactionType;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\HasOne;

/**
 * @property int $id
 * @property string $transaction_type
 * @property string $request_type
 * @property int $reference_transaction_id
 * @property int $credit_card_id
 * @property string $credit_card_number
 * @property int $transaction_amount
 * @property int $balance_before_tr
 * @property int $balance_after_tr
 * @property string $tracking_code
 * @property string $description
 * @property string $created_at
 * @property string $updated_at
 * @property CreditCard $creditCard
 * @property Commission $commission
 */
class Transaction extends Model
{
    use HasFactory;

    /**
     * The attributes that are mass assignable.
     *
     * @var array<int, string>
     */
    protected $fillable = [
        'transaction_type',
        'request_type',
        'reference_transaction_id',
        'credit_card_id',
        'credit_card_number',
        'transaction_amount',
        'balance_before_tr',
        'balance_after_tr',
        'tracking_code',
        'description',
        // Just for test and factory, it should not be here
        'created_at'
    ];

    /**
     * The attributes that should be cast.
     *
     * @var array<string, string>
     */
    protected $casts = [
        'transaction_type' => TransactionType::class,
        'request_type' => TransactionRequestType::class,
    ];

    protected static function booted(): void
    {
        static::creating(function (Transaction $transaction) {
            $transaction->tracking_code = uniqid();
        });
    }

    public function creditCard(): BelongsTo
    {
        return $this->belongsTo(CreditCard::class);
    }

    public function commission(): HasOne
    {
        return $this->hasOne(Commission::class);
    }

    public function withdrawTransaction(): BelongsTo
    {
        return $this->belongsTo(Transaction::class, 'reference_transaction_id', 'id');
    }

    public function depositTransaction(): HasOne
    {
        return $this->hasOne(Transaction::class, 'reference_transaction_id', 'id');
    }
}
