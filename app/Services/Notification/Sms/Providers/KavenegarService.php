<?php

namespace App\Services\Notification\Sms\Providers;

use Illuminate\Support\Facades\Log;
use Kavenegar\Exceptions\ApiException;
use Kavenegar\Exceptions\HttpException;
use Kavenegar\KavenegarApi;

class KavenegarService implements SmsServiceProviderInterface
{

    public function send(string $mobile, string $text)
    {
        $api_key = config('app_config.sms_provider.kavenegar.api_key');
        $lineNumber = config('app_config.kavenegar.lineNumber');
        try {
            $api = new KavenegarApi($api_key);
            $api->Send($lineNumber, $mobile, $text);
        } catch (ApiException|HttpException $e) {
            // send on sentry
            Log::emergency($e->getMessage());
        }
    }
}
