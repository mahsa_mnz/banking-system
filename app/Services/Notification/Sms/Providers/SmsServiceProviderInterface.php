<?php

namespace App\Services\Notification\Sms\Providers;

interface SmsServiceProviderInterface
{
    public function send(string $mobile, string $text);
}
