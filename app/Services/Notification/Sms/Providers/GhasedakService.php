<?php

namespace App\Services\Notification\Sms\Providers;

use Ghasedak\Exceptions\ApiException;
use Ghasedak\Exceptions\HttpException;
use Ghasedak\GhasedakApi;
use Illuminate\Support\Facades\Log;

class GhasedakService implements SmsServiceProviderInterface
{

    public function send(string $mobile, string $text)
    {
        $api_key = config('app_config.sms_provider.ghasedak.api_key');
        $lineNumber = config('app_config.ghasedak.lineNumber');

        try {
            $api = new GhasedakApi($api_key);
            $api->SendSimple($mobile, $text, $lineNumber);
        } catch (ApiException|HttpException $e) {
            // send on sentry
            Log::emergency($e->getMessage());
        }
    }
}
