<?php

namespace App\Services\Notification\Sms\Providers;

use App\Enum\SmsProvider;
use InvalidArgumentException;
use function trans;

class SmsProviderStrategyContext
{

    private SmsServiceProviderInterface $provider;

    public function __construct(string $providerName)
    {
        $this->provider = match ($providerName) {
            SmsProvider::KAVENEGAR->value => new KavenegarService(),
            SmsProvider::GHASEDAK->value => new GhasedakService(),
            default => throw new InvalidArgumentException(trans('errors.sms_provider.invalid.provider_name')),
        };
    }

    public function send(string $mobile, string $text)
    {
        $this->provider->send($mobile, $text);
    }
}
