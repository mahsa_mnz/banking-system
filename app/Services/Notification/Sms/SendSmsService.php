<?php

namespace App\Services\Notification\Sms;

use App\Models\User;
use App\Repositories\Contracts\MessageTemplateRepositoryInterface;
use App\Services\Notification\Sms\Providers\SmsProviderStrategyContext;
use Illuminate\Support\Facades\Blade;
use InvalidArgumentException;

class SendSmsService
{

    public function __construct(private MessageTemplateRepositoryInterface $templateRepository)
    {
    }

    public function sendWithProvider(string $templateName, User $user, array $tokens): void
    {
        $template = $this->templateRepository->findByName($templateName);

        foreach ($template->tokens as $key) {
            if (!array_key_exists($key, $tokens)) {
                throw new InvalidArgumentException('Invalid tokens');
            }
        }

        $context = new SmsProviderStrategyContext($template->provider);

        $text = Blade::render($template->message, $tokens);

        $context->send($user->mobile, $text);
    }
}
