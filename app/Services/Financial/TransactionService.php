<?php

namespace App\Services\Financial;

use App\Enum\TransactionRequestType;
use App\Models\Transaction;
use App\Repositories\Contracts\TransactionRepositoryInterface;

class TransactionService
{

    public function __construct(private TransactionRepositoryInterface $transactionRepository)
    {
    }

    public function createWithdrawTransaction(
        int $cardId,
        string $cardNumber,
        int $amount,
        int $preBalance,
        int $afterBalance,
        bool $commission = false,
        $request_type = TransactionRequestType::WITHDRAW
    ): Transaction {
        $wage = 0;
        if ($commission) {
            $wage = config('app_config.wage.amount') ?? 0;
        }

        return $this->transactionRepository->createWithdrawTransaction(
            $cardId,
            $cardNumber,
            $amount,
            $preBalance,
            $afterBalance,
            $wage,
            $request_type
        );
    }

    public function createDepositTransaction(
        int $cardId,
        string $cardNumber,
        int $amount,
        int $preBalance,
        int $afterBalance,
        int $referenceId = null
    ): Transaction {
        return $this->transactionRepository->createDepositTransaction(
            $cardId,
            $cardNumber,
            $amount,
            $preBalance,
            $afterBalance,
            $referenceId
        );
    }
}
