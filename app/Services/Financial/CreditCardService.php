<?php

namespace App\Services\Financial;

use App\Models\CreditCard;
use App\Repositories\Contracts\CreditCardRepositoryInterface;

class CreditCardService
{
    public function __construct(private CreditCardRepositoryInterface $creditCardRepository)
    {
    }

    public function getCreditCardWithNumber(string $number): ?CreditCard
    {
        return $this->creditCardRepository->findByNumber($number, true, true);
    }
}
