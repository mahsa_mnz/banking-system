<?php

namespace App\Services\Financial;

use App\Models\Account;
use App\Repositories\Contracts\AccountRepositoryInterface;
use Illuminate\Database\Eloquent\Collection;
use Symfony\Component\HttpKernel\Exception\BadRequestHttpException;

class AccountService
{

    public function __construct(private AccountRepositoryInterface $accountRepository)
    {
    }

    public function getAccountWithId(int $id, bool $lock = false): Account
    {
        return $this->accountRepository->getAccountWithId($id, $lock);
    }

    public function getAccountsWithId(array $ids, bool $lock = false): Collection|array
    {
        return $this->accountRepository->getAccountsWithId($ids, $lock);
    }

    public function withdrawFromAccount(Account $account, int $amount, bool $wage = false): array
    {
        if ($wage) {
            $amount += config('app_config.wage.amount') ?? 0;
        }

        if ($account->balance < $amount) {
            throw new BadRequestHttpException(trans('errors.financial.account.insufficient_balance'));
        }

        $newBalance = $account->balance - $amount;

        $this->accountRepository->updateBalance($account->id, $newBalance);

        return [
            $account->balance, $newBalance
        ];
    }

    public function depositToAccount(Account $account, int $amount): array
    {
        $newBalance = $account->balance + $amount;

        $this->accountRepository->updateBalance($account->id, $newBalance);

        return [
            $account->balance, $newBalance
        ];
    }
}
