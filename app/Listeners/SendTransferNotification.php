<?php

namespace App\Listeners;

use App\Enum\MessageTemplateName;
use App\Events\TransferDone;
use App\Services\Notification\Sms\SendSmsService;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Queue\InteractsWithQueue;

class SendTransferNotification implements ShouldQueue
{
    use InteractsWithQueue;

    /**
     * Create the event listener.
     */
    public function __construct(private SendSmsService $smsService)
    {
        //
    }

    /**
     * Handle the event.
     */
    public function handle(TransferDone $event): void
    {
        $this->smsService->sendWithProvider(
            MessageTemplateName::TRANSFER_WITHDRAW->value,
            $event->getFromCreditCard()->account->owner,
            [
                'amount' => $event->getWithdrawTransaction()->transaction_amount,
                'to_card' => $event->getToCreditCard()->number,
                'from_account' => $event->getFromCreditCard()->account->number,
                'after_balance' => convertNumberToHumanReadable($event->getWithdrawTransaction()->balance_after_tr),
            ]
        );

        $this->smsService->sendWithProvider(
            MessageTemplateName::TRANSFER_DEPOSIT->value,
            $event->getToCreditCard()->account->owner,
            [
                'amount' => $event->getDepositTransaction()->transaction_amount,
                'card' => convertCardNumberToHidden($event->getFromCreditCard()->number),
                'after_balance' => convertNumberToHumanReadable($event->getDepositTransaction()->balance_after_tr),
            ]
        );
    }
}
