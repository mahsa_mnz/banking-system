<?php

namespace App\Actions\Financial;

use App\Enum\TransactionRequestType;
use App\Events\TransferDone;
use App\Models\Transaction;
use App\Models\User;
use App\Services\Financial\AccountService;
use App\Services\Financial\CreditCardService;
use App\Services\Financial\TransactionService;
use App\Validations\TransferValidation;
use Exception;
use Illuminate\Support\Facades\DB;

class TransferAction
{

    public function __construct(
        private CreditCardService $cardService,
        private AccountService $accountService,
        private TransactionService $transactionService
    ) {
    }

    /**
     * @throws Exception
     */
    public function transfer(User $requesterUser, array $data): Transaction
    {
        $fromCreditCard = $this->cardService->getCreditCardWithNumber($data['from_card_number']);
        $toCreditCard = $this->cardService->getCreditCardWithNumber($data['to_card_number']);

        (new TransferValidation(
            $fromCreditCard,
            $toCreditCard,
            $data['amount'],
            $requesterUser->id,
            $data['from_cvv2'],
            $data['from_password']
        ))->validate();

        // Run these actions as a single transaction
        try {
            DB::beginTransaction();

            // Lock the accounts
            $accounts = $this->accountService->getAccountsWithId([
                $fromCreditCard->account_id, $toCreditCard->account_id
            ], true);

            // withdraw from account with wage
            [$previousBalance, $newBalance] = $this->accountService->withdrawFromAccount(
                $accounts[0],
                $data['amount'],
                true
            );

            // create withdraw transaction and get before and after balance
            $withdrawTransaction = $this->transactionService->createWithdrawTransaction(
                $fromCreditCard->id,
                $fromCreditCard->number,
                $data['amount'],
                $previousBalance,
                $newBalance,
                true,
                TransactionRequestType::TRANSFER
            );

            // deposit to account and get before and after balance
            [$previousBalance, $newBalance] = $this->accountService->depositToAccount(
                $accounts[1],
                $data['amount']
            );

            // create deposit transaction
            $depositTransaction = $this->transactionService->createDepositTransaction(
                $toCreditCard->id,
                $toCreditCard->number,
                $data['amount'],
                $previousBalance,
                $newBalance,
                $withdrawTransaction->id
            );

            DB::commit();
        } catch (Exception $exception) {
            DB::rollBack();

            throw $exception;
        }

        TransferDone::dispatch($fromCreditCard, $toCreditCard, $withdrawTransaction, $depositTransaction);

        return $withdrawTransaction->load('depositTransaction');
    }
}
