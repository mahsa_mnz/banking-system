<?php

namespace App\Actions\Financial;

use App\Repositories\Contracts\UserRepositoryInterface;
use Illuminate\Database\Eloquent\Collection;

class ReportAction
{

    public function __construct(private UserRepositoryInterface $userRepository)
    {
    }

    public function getList(): Collection|array
    {
        return $this->userRepository->getTopUsersWithLatestTransactions(3, 10, 10);
    }
}
