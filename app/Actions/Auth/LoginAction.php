<?php

namespace App\Actions\Auth;

use App\Exceptions\UnAuthorizedException;
use App\Repositories\Contracts\UserRepositoryInterface;
use Illuminate\Support\Facades\Hash;

class LoginAction
{
    private UserRepositoryInterface $userRepository;

    /**
     * @param UserRepositoryInterface $userRepository
     */
    public function __construct(UserRepositoryInterface $userRepository)
    {
        $this->userRepository = $userRepository;
    }

    public function login(array $loginData)
    {
        $user = $this->userRepository->findUserByMobile($loginData['mobile']);

        if (!$user || !Hash::check($loginData['password'], $user->password)) {
            throw new UnAuthorizedException();
        }

        return $user->createToken($user->mobile.'-AuthToken')->plainTextToken;
    }
}
