<?php

namespace App\Providers;

use App\Repositories\Contracts as Contracts;
use App\Repositories\MySqlEloquent as Repositories;
use Illuminate\Support\ServiceProvider;

class RepositoryServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     */
    public function register(): void
    {
        //
    }

    /**
     * Bootstrap any application services.
     */
    public function boot(): void
    {
        $this->app->bind(
            Contracts\UserRepositoryInterface::class,
            Repositories\UserRepository::class
        );
        $this->app->bind(
            Contracts\CreditCardRepositoryInterface::class,
            Repositories\CreditCardRepository::class
        );
        $this->app->bind(
            Contracts\AccountRepositoryInterface::class,
            Repositories\AccountRepository::class
        );
        $this->app->bind(
            Contracts\TransactionRepositoryInterface::class,
            Repositories\TransactionRepository::class
        );
        $this->app->bind(
            Contracts\MessageTemplateRepositoryInterface::class,
            Repositories\MessageTemplateRepository::class
        );
    }
}
