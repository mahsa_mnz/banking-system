<?php

if (!function_exists('convertPersianToEnNumbers')) {
    /**
     * This function convert persian numbers to english
     * @param string $persianNumberString string containing persian numbers
     */
    function convertPersianToEnNumbers(string $persianNumberString): string
    {
        $persian = ['۰', '۱', '۲', '۳', '۴', '۵', '۶', '۷', '۸', '۹'];
        $num = range(0, 9);
        return str_replace($persian, $num, $persianNumberString);
    }
}

if (!function_exists('convertArabicToEnNumbers')) {
    /**
     * This function convert persian numbers to english
     * @param string $arabicNumberString string containing persian numbers
     */
    function convertArabicToEnNumbers(string $arabicNumberString): string
    {
        $arabic = ['٩', '٨', '٧', '٦', '٥', '٤', '٣', '٢', '١','٠'];
        $num = range(0, 9);
        return str_replace($arabic, $num, $arabicNumberString);
    }
}

if (!function_exists('convertToEnNumbers')) {
    /**
     * This function convert undefined numbers to english
     * @param string $numberString string containing persian or arabic numbers
     */
    function convertToEnNumbers(string $numberString): string
    {
        $convertedFromPersian = convertPersianToEnNumbers($numberString);
        return convertArabicToEnNumbers($convertedFromPersian);
    }
}

if (!function_exists('convertCardNumberToHidden')) {
    /**
     * This function convert card number to the type with hidden numbers
     * @param string $creditCardNumber card number
     */
    function convertCardNumberToHidden(string $creditCardNumber): string
    {
        return substr_replace($creditCardNumber, '-xxxx-xxxx-', 4, 8);
    }
}

if (!function_exists('convertCardNumberToHumanReadable')) {
    /**
     * This function convert card number to human-readable
     * @param string $creditCardNumber card number
     */
    function convertCardNumberToHumanReadable(string $creditCardNumber): string
    {
        return implode('-', str_split($creditCardNumber, 4));
    }
}

if (!function_exists('convertNumberToHumanReadable')) {
    /**
     * This function convert number to human-readable
     * @param string $number number
     */
    function convertNumberToHumanReadable(string $number): string
    {
        return implode(',', str_split($number, 3));
    }
}
