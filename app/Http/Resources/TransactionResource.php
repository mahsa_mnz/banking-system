<?php

namespace App\Http\Resources;

use Illuminate\Http\Request;
use Illuminate\Http\Resources\Json\JsonResource;

class TransactionResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @return array<string, mixed>
     */
    public function toArray(Request $request): array
    {
        return [
            'id' => $this->id,
            'transaction_type' => $this->transaction_type,
            'request_type' => $this->request_type,
            'card_number' => convertCardNumberToHidden($this->credit_card_number),
            'amount' => convertNumberToHumanReadable($this->transaction_amount),
            'new_balance' => $this->balance_after_tr,
            'tracking_code' => $this->tracking_code,
        ];
    }
}
