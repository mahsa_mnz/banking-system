<?php

namespace App\Http\Resources;

use Illuminate\Http\Request;
use Illuminate\Http\Resources\Json\JsonResource;

class TransferResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @return array<string, mixed>
     */
    public function toArray(Request $request): array
    {
        return [
            'transaction_type' => $this->transaction_type,
            'request_type' => $this->request_type,
            'from_card_number' => convertCardNumberToHidden($this->credit_card_number),
            'amount' => convertNumberToHumanReadable($this->transaction_amount),
            'new_balance' => $this->balance_after_tr,
            'from_tracking_code' => $this->tracking_code,
            'to_card_number' => convertCardNumberToHumanReadable($this->depositTransaction->credit_card_number),
            'to_tracking_code' => $this->depositTransaction->tracking_code,
        ];
    }
}
