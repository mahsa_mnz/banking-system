<?php

namespace App\Http\Requests\Financial;

use App\Rules\CreditCardNumber;
use Illuminate\Foundation\Http\FormRequest;

class TransferRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     */
    public function authorize(): bool
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules(): array
    {
        return [
            'from_card_number' => [
                'required',
                'numeric',
                new CreditCardNumber()
            ],
            'from_cvv2' => 'required|numeric|digits_between:3,4',
            'from_password' => 'required|string|min:6',
            'to_card_number' => [
                'required',
                'numeric',
                new CreditCardNumber()
            ],
            'amount' => 'required|numeric|min:1',
        ];
    }

    protected function prepareForValidation()
    {
        $data = $this->only(['from_card_number', 'from_cvv2', 'to_card_number', 'amount']);
        foreach ($data as $key => $value) {
            if ($value) {
                $this->merge([$key => convertToEnNumbers($value)]);
            }
        }
    }
}
