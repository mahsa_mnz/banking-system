<?php

namespace App\Http\Controllers\Auth;

use App\Actions\Auth\LoginAction;
use App\Http\Controllers\Controller;
use App\Http\Requests\Auth\LoginRequest;
use Illuminate\Http\JsonResponse;

class LoginController extends Controller
{
    private LoginAction $loginAction;

    /**
     * @param LoginAction $loginAction
     */
    public function __construct(LoginAction $loginAction)
    {
        $this->loginAction = $loginAction;
    }

    public function __invoke(LoginRequest $request): JsonResponse
    {
        $loginData = $request->validated();
        $result = $this->loginAction->login($loginData);
        return $this->success($result);
    }
}
