<?php

namespace App\Http\Controllers\Financial;

use App\Actions\Financial\TransferAction;
use App\Http\Controllers\Controller;
use App\Http\Requests\Financial\TransferRequest;
use App\Http\Resources\TransferResource;
use Exception;
use Illuminate\Http\JsonResponse;

class TransferController extends Controller
{
    private TransferAction $action;

    /**
     * @param TransferAction $action
     */
    public function __construct(TransferAction $action)
    {
        $this->action = $action;
    }

    /**
     * @throws Exception
     */
    public function __invoke(TransferRequest $request): JsonResponse
    {
        $loggedInUser = $request->user();
        $data = $request->validated();
        $result = $this->action->transfer($loggedInUser, $data);
        return $this->success(new TransferResource($result));
    }
}
