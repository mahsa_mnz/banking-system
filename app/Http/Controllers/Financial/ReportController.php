<?php

namespace App\Http\Controllers\Financial;

use App\Actions\Financial\ReportAction;
use App\Http\Controllers\Controller;
use App\Http\Resources\UserWithTransactionsResource;
use Illuminate\Http\JsonResponse;

class ReportController extends Controller
{

    public function __construct(private ReportAction $reportAction)
    {
    }

    public function __invoke(): JsonResponse
    {
        $result = $this->reportAction->getList();
        return $this->success(UserWithTransactionsResource::collection($result));
    }
}
