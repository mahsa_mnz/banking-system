<?php

namespace App\Http\Controllers;

use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Http\JsonResponse;
use Illuminate\Pagination\LengthAwarePaginator;
use Illuminate\Routing\Controller as BaseController;
use Symfony\Component\HttpFoundation\Response as ResponseAlias;

class Controller extends BaseController
{
    use AuthorizesRequests;
    use ValidatesRequests;

    protected function success($data, $message = '', $meta = null): JsonResponse
    {
        if ($data instanceof LengthAwarePaginator) {
            $temp = $data->toArray();
            $data = $temp['data'];
            unset($temp['data']);
            $meta = $temp;
        }
        return response()->json([
            'message' => $message,
            'data' => $data,
            'meta' => $meta,
        ]);
    }

    protected function created($data, $message = '', $meta = null): JsonResponse
    {
        return response()->json([
            'message' => $message,
            'data' => $data,
            'meta' => $meta,
        ], ResponseAlias::HTTP_CREATED);
    }

    protected function deleted($message = '', $meta = null): JsonResponse
    {
        return response()->json([
            'message' => $message,
            'data' => null,
            'meta' => $meta,
        ], ResponseAlias::HTTP_NO_CONTENT);
    }
}
