<?php

namespace App\Rules;

use Closure;
use Illuminate\Contracts\Validation\ValidationRule;

class CreditCardNumber implements ValidationRule
{
    /**
     * Run the validation rule.
     *
     * @param string $attribute
     * @param mixed $value
     * @param Closure $fail
     */
    public function validate(string $attribute, mixed $value, Closure $fail): void
    {
        if (empty($value) || strlen($value) !== 16) {
            $is_failed = true;
        } else {
            $arrayOfCardNumber = array_map(function ($item) {
                return (int) $item;
            }, str_split($value));
            $totalSum = 0;
            for ($i = 0; $i < 16; $i++) {
                if ($i % 2 === 0) {
                    $totalSum += (($arrayOfCardNumber[$i] * 2 > 9) ?
                        ($arrayOfCardNumber[$i] * 2) - 9 :
                        ($arrayOfCardNumber[$i] * 2));
                } else {
                    $totalSum += $arrayOfCardNumber[$i];
                }
            }
            $is_failed = !($totalSum % 10 === 0);
        }

        if ($is_failed) {
            $fail('validation.curd_number')->translate();
        }
    }
}
