<?php

namespace App\Events;

use App\Models\CreditCard;
use App\Models\Transaction;
use Illuminate\Broadcasting\InteractsWithSockets;
use Illuminate\Foundation\Events\Dispatchable;
use Illuminate\Queue\SerializesModels;

class TransferDone
{
    use Dispatchable;
    use InteractsWithSockets;
    use SerializesModels;

    /**
     * Create a new event instance.
     */
    public function __construct(
        private CreditCard $fromCreditCard,
        private CreditCard $toCreditCard,
        private Transaction $withdrawTransaction,
        private Transaction $depositTransaction
    ) {
        //
    }

    /**
     * @return CreditCard
     */
    public function getFromCreditCard(): CreditCard
    {
        return $this->fromCreditCard;
    }

    /**
     * @return CreditCard
     */
    public function getToCreditCard(): CreditCard
    {
        return $this->toCreditCard;
    }

    /**
     * @return Transaction
     */
    public function getWithdrawTransaction(): Transaction
    {
        return $this->withdrawTransaction;
    }

    /**
     * @return Transaction
     */
    public function getDepositTransaction(): Transaction
    {
        return $this->depositTransaction;
    }
}
