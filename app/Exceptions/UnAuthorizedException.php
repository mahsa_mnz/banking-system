<?php

namespace App\Exceptions;

use Symfony\Component\HttpKernel\Exception\UnauthorizedHttpException;

class UnAuthorizedException extends UnauthorizedHttpException
{
    public function __construct()
    {
        $message = trans('auth.failed');
        parent::__construct('Bearer', $message);
    }
}
