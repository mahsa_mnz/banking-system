<?php

namespace App\Enum;

enum TransactionType: string
{
    case INCREASE = 'increase';
    case DECREASE = 'decrease';
}
