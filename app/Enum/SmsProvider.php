<?php

namespace App\Enum;

enum SmsProvider: string
{
    case KAVENEGAR = 'kavenegar';
    case GHASEDAK = 'ghasedak';
}
