<?php

namespace App\Enum;

enum MessageTemplateName: string
{
    case TRANSFER_WITHDRAW = 'transfer_withdraw';
    case TRANSFER_DEPOSIT = 'transfer_deposit';
}
