<?php

namespace App\Enum;

enum TransactionRequestType: string
{
    case WITHDRAW = 'withdraw';
    case DEPOSIT = 'deposit';
    case TRANSFER = 'transfer';
}
