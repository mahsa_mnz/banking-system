<?php

namespace App\Validations;

use App\Models\CreditCard;
use App\Validations\Handlers\AccountValidation;
use App\Validations\Handlers\AuthorityValidation;
use App\Validations\Handlers\CreditCardValidation;
use App\Validations\Handlers\WithdrawValidation;
use Symfony\Component\HttpKernel\Exception\BadRequestHttpException;

class TransferValidation
{

    public function __construct(
        private CreditCard $from,
        private CreditCard $to,
        private int $amount,
        private int $user_id,
        private string $cvv2,
        private string $password
    ) {
    }
    /**
     * Validate Transfer action with Chain of responsibility
    */
    public function validate()
    {
        $authorityValidator = new AuthorityValidation($this->user_id, $this->password, $this->cvv2);
        $creditCardValidator = new CreditCardValidation();
        $accountValidator = new AccountValidation();
        $withdrawValidator = new WithdrawValidation($this->amount);

        $authorityValidator->setNext($creditCardValidator);
        $creditCardValidator->setNext($accountValidator);
        $accountValidator->setNext($withdrawValidator);

        $authorityValidator->validate($this->from);

        $accountValidator2 = new AccountValidation();
        $creditCardValidator->setNext($accountValidator2);

        $creditCardValidator->validate($this->to);

        if ($this->from->account_id == $this->to->account_id) {
            throw new BadRequestHttpException(trans('errors.financial.transfer.same_source'));
        }
    }
}
