<?php

namespace App\Validations\Handlers;

use App\Models\CreditCard;
use Illuminate\Support\Facades\Hash;
use Symfony\Component\HttpKernel\Exception\BadRequestHttpException;

class AuthorityValidation extends ValidationHandler
{
    private int $user_id;
    private string $password;
    private string $cvv2;

    /**
     * @param int $user_id
     * @param string $password
     * @param string $cvv2
     */
    public function __construct(int $user_id, string $password, string $cvv2)
    {
        $this->user_id = $user_id;
        $this->password = $password;
        $this->cvv2 = $cvv2;
    }


    public function validate(CreditCard $creditCard): bool
    {
        if (!$this->isBelongsToAUser($creditCard)) {
            throw new BadRequestHttpException(trans('errors.financial.credit_card.invalid.account_owner'));
        } elseif (!$this->isCredentialsValid($creditCard)) {
            throw new BadRequestHttpException(trans('errors.financial.credit_card.invalid.credentials'));
        } elseif (isset($this->next)) {
            return $this->next->validate($creditCard);
        } else {
            return true;
        }
    }

    private function isBelongsToAUser(CreditCard $creditCard): bool
    {
        $account = $creditCard->account;
        $accountOwner = $account->owner;

        return $accountOwner->id == $this->user_id;
    }

    private function isCredentialsValid(CreditCard $creditCard): bool
    {
        return $creditCard->cvv == $this->cvv2 and Hash::check($this->password, $creditCard->password);
    }
}
