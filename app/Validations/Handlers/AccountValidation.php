<?php

namespace App\Validations\Handlers;

use App\Models\Account;
use App\Models\CreditCard;
use Symfony\Component\HttpKernel\Exception\BadRequestHttpException;

class AccountValidation extends ValidationHandler
{

    public function validate(CreditCard $creditCard): bool
    {
        /** @var Account $account */
        $account = $creditCard->account;

        if (!$this->isActive($account)) {
            throw new BadRequestHttpException(trans('errors.financial.account.in_active'));
        } elseif (isset($this->next)) {
            return $this->next->validate($creditCard);
        } else {
            return true;
        }
    }

    private function isActive(Account $account): bool
    {
        return $account->is_active;
    }
}
