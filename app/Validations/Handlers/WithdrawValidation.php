<?php

namespace App\Validations\Handlers;

use App\Models\Account;
use App\Models\CreditCard;
use Symfony\Component\HttpKernel\Exception\BadRequestHttpException;
use function config;

class WithdrawValidation extends ValidationHandler
{

    public function __construct(private int $amount)
    {
    }

    public function validate(CreditCard $creditCard): bool
    {
        /** @var Account $account */
        $account = $creditCard->account;

        if (!$this->isInValidRange($creditCard)) {
            throw new BadRequestHttpException(trans('errors.financial.credit_card.invalid.range'));
        } elseif (!$this->hasSufficientBalance($account)) {
            throw new BadRequestHttpException(trans('errors.financial.account.insufficient_balance'));
        } elseif (isset($this->next)) {
            return $this->next->validate($creditCard);
        } else {
            return true;
        }
    }

    private function hasSufficientBalance(Account $account): bool
    {
        return $account->balance > ($this->amount + config('app_config.wage.amount'));
    }

    private function isInValidRange(CreditCard $creditCard): bool
    {
        return $creditCard->min_transaction_limit <= $this->amount and
            $this->amount <= $creditCard->max_transaction_limit;
    }
}
