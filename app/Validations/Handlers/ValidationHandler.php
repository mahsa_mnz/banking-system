<?php

namespace App\Validations\Handlers;

use App\Models\CreditCard;

abstract class ValidationHandler
{
    protected ValidationHandler $next;

    /**
     * @param ValidationHandler $next
     */
    public function setNext(ValidationHandler $next): void
    {
        $this->next = $next;
    }

    abstract public function validate(CreditCard $creditCard):bool;
}
