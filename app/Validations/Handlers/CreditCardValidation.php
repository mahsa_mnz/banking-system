<?php

namespace App\Validations\Handlers;

use App\Models\CreditCard;
use Symfony\Component\HttpKernel\Exception\BadRequestHttpException;
use function now;

class CreditCardValidation extends ValidationHandler
{
    public function validate(CreditCard $creditCard): bool
    {
        if (!$this->isActive($creditCard)) {
            throw new BadRequestHttpException(trans('errors.financial.credit_card.in_active'));
        } elseif ($this->isExpired($creditCard)) {
            throw new BadRequestHttpException(trans('errors.financial.credit_card.expired'));
        } elseif (isset($this->next)) {
            return $this->next->validate($creditCard);
        } else {
            return true;
        }
    }

    private function isActive(CreditCard $creditCard): bool
    {
        return $creditCard->is_active;
    }

    private function isExpired(CreditCard $creditCard): bool
    {
        return $creditCard->expire_date <= now();
    }
}
