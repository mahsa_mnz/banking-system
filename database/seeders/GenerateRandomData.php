<?php

namespace Database\Seeders;

use App\Models\Account;
use App\Models\CreditCard;
use App\Models\Transaction;
use App\Models\User;
use Illuminate\Database\Seeder;

class GenerateRandomData extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        for ($i = 1; $i <= rand(10, 20); $i++) {
            User::factory()
                ->has(
                    Account::factory()->has(
                        CreditCard::factory()->has(
                            Transaction::factory()->count(rand(1, 20))
                        )->active()->count(rand(1, 3))
                    )->active()->count(2)
                )
                ->create();
        }
    }
}
