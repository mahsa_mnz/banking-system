<?php

namespace Database\Seeders;

use App\Models\Account;
use App\Models\User;
use Illuminate\Database\Seeder;

class GenerateRealData extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        User::factory()
            ->has(
                Account::factory()
                    ->hasCreditCards([
                            'number' => '6274884000163587',
                            'cvv' => '1234',
                            'expire_date' => now()->addYears(4),
                            'min_transaction_limit' => 1000,
                            'max_transaction_limit' => 50000000,
                            'password' => 'Mahsa123',
                            'is_active' => true
                        ])
                    ->active()
            )
            ->create(['mobile' => '09129374819', 'password' => '123456']);

        User::factory()
            ->has(
                Account::factory()->hasCreditCards(['number' => '6037991786212435'])->active()
            )
            ->create();
    }
}
