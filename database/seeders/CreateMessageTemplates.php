<?php

namespace Database\Seeders;

use App\Enum\SmsProvider;
use App\Enum\MessageTemplateName;
use App\Models\MessageTemplate;
use Illuminate\Database\Seeder;

class CreateMessageTemplates extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        MessageTemplate::query()->create([
            'provider' => SmsProvider::KAVENEGAR,
            'template_name' => MessageTemplateName::TRANSFER_WITHDRAW,
            'message' => 'برداشت مبلغ {{$amount}} بابت انتقال به کارت {{$to_card}}
از حساب {{$from_account}}
مانده : {{$after_balance}}',
            'tokens' => [
                'amount', 'to_card', 'from_account', 'after_balance'
            ]
        ]);

        MessageTemplate::query()->create([
            'provider' => SmsProvider::GHASEDAK,
            'template_name' => MessageTemplateName::TRANSFER_DEPOSIT,
            'message' => 'واریز مبلغ {{$amount}} از کارت {{$card}}
مانده : {{$after_balance}}',
            'tokens' => [
                'amount', 'card', 'after_balance'
            ]
        ]);
    }
}
