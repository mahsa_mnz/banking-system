<?php

namespace Database\Factories;

use App\Enum\TransactionRequestType;
use App\Enum\TransactionType;
use Illuminate\Database\Eloquent\Factories\Factory;

/**
 * @extends \Illuminate\Database\Eloquent\Factories\Factory<\App\Models\Transaction>
 */
class TransactionFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array<string, mixed>
     */
    public function definition(): array
    {
        $balance = fake()->numberBetween(1000000, 100000000);

        return [
            'transaction_type' => fake()->randomElement(TransactionType::cases()),
            'request_type' => fake()->randomElement(TransactionRequestType::cases()),
            'transaction_amount' => $amount = fake()->numberBetween(10000, $balance),
            'balance_before_tr' => $balance,
            'balance_after_tr' => $balance - $amount,
            'tracking_code' => fake()->randomNumber(8),
            'credit_card_number' => fake()->numerify('################'),
            'created_at' => now()->subMinutes(fake()->numberBetween(1, 60))
        ];
    }
}
