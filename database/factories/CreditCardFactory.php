<?php

namespace Database\Factories;

use Illuminate\Database\Eloquent\Factories\Factory;

/**
 * @extends \Illuminate\Database\Eloquent\Factories\Factory<\App\Models\CreditCard>
 */
class CreditCardFactory extends Factory
{
    /**
     * The current password being used by the factory.
     */
    protected static ?string $password;

    /**
     * Define the model's default state.
     *
     * @return array<string, mixed>
     */
    public function definition(): array
    {
        return [
            'number' => fake()->numerify('################'),
            'is_active' => fake()->boolean,
            'cvv' => fake()->randomNumber(4, true),
            'expire_date' => now()->addMonths(fake()->numberBetween(1, 12)),
            'min_transaction_limit' => $min = fake()->numberBetween(1000),
            'max_transaction_limit' => fake()->numberBetween($min),
            'password' => fake()->randomNumber(6)
        ];
    }

    /**
     * Indicate that the card is active.
     */
    public function active(): static
    {
        return $this->state(fn (array $attributes) => [
            'is_active' => true,
        ]);
    }

    /**
     * Indicate that the card is not active.
     */
    public function inActive(): static
    {
        return $this->state(fn (array $attributes) => [
            'is_active' => false,
        ]);
    }

    /**
     * Indicate that the card is expired.
     */
    public function expired(): static
    {
        return $this->state(fn (array $attributes) => [
            'expire_date' => now()->subDays(fake()->randomDigit()),
        ]);
    }
}
