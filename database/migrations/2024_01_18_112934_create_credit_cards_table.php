<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('credit_cards', function (Blueprint $table) {
            $table->id();
            $table->foreignId('account_id');
            $table->string('number', 16)->unique();
            $table->boolean('is_active')->default(true);
            $table->string('cvv', 4);
            $table->date('expire_date');
            $table->string('password');
            $table->unsignedBigInteger('min_transaction_limit');
            $table->unsignedBigInteger('max_transaction_limit');
            $table->timestamps();

            $table->foreign('account_id')->on('accounts')->references('id')
                ->onUpdate('cascade')->onDelete('restrict');
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('credit_cards');
    }
};
