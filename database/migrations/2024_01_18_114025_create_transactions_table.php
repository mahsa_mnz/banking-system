<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('transactions', function (Blueprint $table) {
            $table->id();
            $table->string('transaction_type');
            $table->string('request_type');
            $table->foreignId('reference_transaction_id')->nullable();
            $table->foreignId('credit_card_id');
            $table->string('credit_card_number', 16);
            $table->unsignedBigInteger('transaction_amount');
            $table->unsignedBigInteger('balance_before_tr');
            $table->unsignedBigInteger('balance_after_tr');
            $table->string('tracking_code');
            $table->text('description')->nullable();
            $table->timestamps();

            $table->foreign('credit_card_id')->on('credit_cards')->references('id')
                ->onUpdate('cascade')->onDelete('restrict');

            $table->foreign('reference_transaction_id')->on('transactions')->references('id')
                ->onUpdate('cascade')->onDelete('restrict');
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('transactions');
    }
};
