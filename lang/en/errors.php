<?php

return [
  'financial' => [
      'account' => [
          'in_active' => 'The account is not active',
          'insufficient_balance' => 'The balance of the account isn\'t sufficient',
      ],
      'credit_card' => [
          'in_active' => 'The credit card is not active',
          'expired' => 'The credit card is expired',
          'invalid' => [
              'account_owner' => 'The selected card isn\'t belongs to the requester',
              'credentials' => 'The password or cvv2 is not valid',
              'range' => 'The requested amount is not in a valid range of the credit card',
          ]
      ],
      'transfer' => [
          'same_source' => 'Transfer money from the cards with same account couldn\'t be done',
      ],
      'sms_provider' => [
          'invalid' => [
              'provider_name' => 'specified Sms Provider name is not supported'
          ]
      ]
  ]
];
