<?php

use App\Http\Controllers\Auth as AuthDir;
use App\Http\Controllers\Financial as FinancialDir;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider and all of them will
| be assigned to the "api" middleware group. Make something great!
|
*/

Route::post('login', AuthDir\LoginController::class)->name('login');

Route::middleware('auth:sanctum')->group(function () {
    Route::post('transfer', FinancialDir\TransferController::class)->name('transfer');
});

Route::get('report', FinancialDir\ReportController::class)->name('report');
