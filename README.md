# Banking system

### Description
This is a test project for examining my development skills

Service responsibilities :
- Transfer money from a card number to another card
- Send Sms to the transaction  related persons
- Get a list of users with the most activities within 10 minutes


### Owner
<h6>1. Mahsa Monazami : mahsa.mnz@gmail.com</h6>

## Technologies
- Language : PHP 8.2
- Framework : Laravel 10
- Database : Mysql 8
- Cache : Redis 5


## Deploy
Docker technology has been used for deploying current microservice. So for creating the required containers run the command below:

``cp .env.example .env``

``docker-compose up -d``

There are some libraries and packages. Run the command below to install them with composer:

``docker-compose exec banking_system_app composer install``

Run the command below to generate the keys:

``docker-compose exec banking_system_app php artisan key:generate``

Then execute this command to create all tables and schemas:

``docker-compose exec banking_system_app php artisan migrate``

``docker-compose exec banking_system_app php artisan db:seed``


###API doc

A postman collection is used for the APIs, and it could be downloaded from [here](./postmanCollection.json)

