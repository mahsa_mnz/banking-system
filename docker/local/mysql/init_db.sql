# create databases
CREATE DATABASE IF NOT EXISTS `banking_system_test`;

# grant rights
GRANT ALL ON banking_system_test.* TO 'banking_system_user'@'%';
